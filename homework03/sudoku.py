import random

def read_sudoku(filename):
    """ Прочитать Судоку из указанного файла """
    digits = [c for c in open(filename).read() if c in '123456789.']
    grid = group(digits, 9)
    return grid


def display(values):
    """Вывод Судоку """
    width = 2
    line = '+'.join(['-' * (width * 3)] * 3)
    for row in range(9):
        print(''.join(values[row][col].center(width) + ('|' if str(col) in '25' else '') for col in range(9)))
        if str(row) in '25':
            print(line)
    print()


def group(values, n):
    """
    Сгруппировать значения values в список, состоящий из списков по n элементов

    >>> group([1,2,3,4], 2)
    [[1, 2], [3, 4]]
    >>> group([1,2,3,4,5,6,7,8,9], 3)
    [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    """
    count = len(values)
    groups = []
    for i in range(count // n):
        groups.append(values[i * n:i * n + n])
    return groups


def get_row(values, pos):
    """ Возвращает все значения для номера строки, указанной в pos

    >>> get_row([['1', '2', '.'], ['4', '5', '6'], ['7', '8', '9']], (0, 0))
    ['1', '2', '.']
    >>> get_row([['1', '2', '3'], ['4', '.', '6'], ['7', '8', '9']], (1, 0))
    ['4', '.', '6']
    >>> get_row([['1', '2', '3'], ['4', '5', '6'], ['.', '8', '9']], (2, 0))
    ['.', '8', '9']
    """
    return values[pos[0]]


def get_col(values, pos):
    """ Возвращает все значения для номера столбца, указанного в pos

    >>> get_col([['1', '2', '.'], ['4', '5', '6'], ['7', '8', '9']], (0, 0))
    ['1', '4', '7']
    >>> get_col([['1', '2', '3'], ['4', '.', '6'], ['7', '8', '9']], (0, 1))
    ['2', '.', '8']
    >>> get_col([['1', '2', '3'], ['4', '5', '6'], ['.', '8', '9']], (0, 2))
    ['3', '6', '9']
    """
    col = []
    for row in values:
        col.append(row[pos[1]])
    return col


def get_block(values, pos):
    """ Возвращает все значения из квадрата, в который попадает позиция pos """
    row, col = pos
    row, col = row - (row % 3), col - (col % 3)
    block = []
    for rows in values[row:row + 3]:
        for cols in rows[col:col + 3]:
            block.append(cols)
    return block


def find_empty_positions(grid):
    """ Найти первую свободную позицию в пазле

    >>> find_empty_positions([['1', '2', '.'], ['4', '5', '6'], ['7', '8', '9']])
    (0, 2)
    >>> find_empty_positions([['1', '2', '3'], ['4', '.', '6'], ['7', '8', '9']])
    (1, 1)
    >>> find_empty_positions([['1', '2', '3'], ['4', '5', '6'], ['.', '8', '9']])
    (2, 0)
    """
    for row in range(len(grid)):
        for col in range(len(grid)):
            if grid[row][col] == '.':
                return row, col
    pass


def find_possible_values(grid, pos):
    """ Вернуть все возможные значения для указанной позиции """
    row_set = set(get_row(grid, pos))
    col_set = set(get_col(grid, pos))
    block_set = set(get_block(grid, pos))
    character_set = set(str(c) for c in range(1, len(grid)+1))
    character_set.add('.')
    all_values = row_set | col_set | block_set
    possible_values = character_set - all_values
    return list(possible_values)


def solve(grid):
    """ Решение пазла, заданного в grid """
    """ Как решать Судоку?
        1. Найти свободную позицию
        2. Найти все возможные значения, которые могут находиться на этой позиции
        3. Для каждого возможного значения:
            3.1. Поместить это значение на эту позицию
            3.2. Продолжить решать оставшуюся часть пазла
    """
    pos = find_empty_positions(grid)
    if pos is None:
        return grid
    row, col = pos
    possible_values = find_possible_values(grid, pos)
    for value in possible_values:
        grid[row][col] = value
        solve(grid)
        if find_empty_positions(grid) is None:
            return grid
    grid[row][col] = '.'
    return grid


def check_solution(solution):
    """ Если решение solution верно, то вернуть True, в противном случае False """
    values = []
    for row in solution:
        values = values + row
    for i in range(1, len(grid)+1):
        if values.count(str(i)) != len(solution):
            return False
    return True


def generate_sudoku(N):
    positions = []
    grid = []
    for row in range(9):
        grid.append([])
        for col in range(9):
            grid[row].append('.')
            positions.append((row, col))
    for i in range(N):
        pos = positions.pop(random.randint(0, len(positions)-1))
        possible_values = find_possible_values(grid, pos)
        if len(possible_values) > 0:
            grid[pos[0]][pos[1]] = possible_values[random.randint(0, len(possible_values)-1)]
    return grid


if __name__ == '__main__':
    for fname in ['puzzle1.txt', 'puzzle2.txt', 'puzzle3.txt']:
        grid = read_sudoku(fname)
        display(grid)
        solution = solve(grid)
        display(solution)
