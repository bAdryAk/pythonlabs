import requests
from datetime import date
from datetime import datetime
from time import sleep
import plotly
import plotly.plotly as py
import plotly.graph_objs as go

domain = "https://api.vk.com/method"
access_token = "c1e3bf534ed567286fd84ba2bbdb5d4dd4503890ae31b6328d28980541e6660b6718e3299240f9e320a69"

def get_friends(user_id, fields):
    """ Returns a list of user IDs or detailed information about a user's friends """
    assert isinstance(user_id, int), "user_id must be positive integer"
    assert isinstance(fields, str), "fields must be string"
    assert user_id > 0, "user_id must be positive integer"

    query_params = {
        'domain': domain,
        'access_token': access_token,
        'user_id': user_id,
        'fields': fields
    }

    query = "{domain}/friends.get?access_token={access_token}&user_id={user_id}&fields={fields}&v=5.62".format(
        **query_params)
    response = requests.get(query)
    return response.json()


def age_predict(user_id):
    """
    >>> age_predict(6125170)
    28
    """
    assert isinstance(user_id, int), "user_id must be positive integer"
    assert user_id > 0, "user_id must be positive integer"
    json = get_friends(user_id, 'bdate')
    if 'response' not in json:
        return
    friends = json['response']['items']
    years_sum = 0
    count = 0
    for friend in friends:
        if 'bdate' in friend:
            values = friend['bdate'].split('.')
            if len(values) == 3:
                years_sum += int(values[2])
                count += 1
    return date.today().year - years_sum // count


def messages_get_history(user_id, offset=0, count=20):
    assert isinstance(user_id, int), "user_id must be positive integer"
    assert user_id > 0, "user_id must be positive integer"
    assert isinstance(offset, int), "offset must be positive integer"
    assert offset >= 0, "user_id must be positive integer"
    assert count >= 0, "user_id must be positive integer"

    query_params = {
        'domain': domain,
        'access_token': access_token,
        'user_id': user_id,
        'offset': offset,
        'count': count
    }

    query = "{domain}/messages.getHistory?access_token={access_token}&user_id={user_id}&offset={offset}&count={count}&v=5.62".format(
        **query_params)
    response = requests.get(query)

    return response.json()


def get_all_messages(user_id, max_count=1000):
    messages = []
    count_requests = 0
    offset = 0
    count = 200
    while True:
        json = messages_get_history(user_id, offset, count)
        if  ('response' in json and len(json['response']['items']) != 0):
            messages += json['response']['items']
            if len(messages) >= max_count:
                return messages
            count_requests += 1
            offset += count
            if count_requests > 2:
                sleep(1.5)
                count_requests = 0
        else:
            break
    return messages


def count_dates_from_messages(messages):
    dates_count = {}
    for message in messages:
        year_key = datetime.fromtimestamp(message['date']).strftime("%Y-%m-%d")
        if year_key in dates_count:
            dates_count[year_key] += 1
        else:
            dates_count[str(year_key)] = 1
    return dates_count

if __name__ == "__main__":
    print(age_predict(3206599))
    messages = get_all_messages(3206599, max_count=5000)
    dates = count_dates_from_messages(messages)
    x = []
    y = []
    for key, value in dates.items():
        x.append(datetime.strptime(key, "%Y-%m-%d"))
        y.append(value)
    plotly.tools.set_credentials_file(username='bAdryAk', api_key='Jtz7JZHF1aYZYvCTN0mE')
    data = [go.Scatter(x=x, y=y)]
    py.iplot(data)
    pass

