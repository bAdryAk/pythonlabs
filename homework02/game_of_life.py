import random
import pygame
from pygame.locals import *


class Cell:

    def __init__(self, x, y, state=0):
        self.x = x
        self.y = y
        self.state = state

    def is_alive(self):
        return self.state

    def __str__(self):
        return "x={} y={} state={}".format(self.x, self.y, self.is_alive())


class CellList:
    def __init__(self, nrow, ncol):
        self.row_number = nrow
        self.col_number = ncol
        self.cells = []
        for i in range(0, nrow):
            for j in range(0, ncol):
                self.cells.append(Cell(i, j))
        self.element_number = 0
        pass

    def update(self):
        new_cells = []
        for cell in self.cells:
            new_cell = Cell(cell.x, cell.y, state=cell.state)
            alived = 0
            for neighbour in self.get_neighbours(cell):
                alived += neighbour.state
            if alived != 2 and alived != 3:
                new_cell.state = 0
            if alived == 3:
                new_cell.state = 1
            new_cells.append(new_cell)
        self.cells = new_cells
        pass

    def get_neighbours(self, cell):
        neighbours = []
        for i_neighbours in range(cell.x - 1, cell.x + 2):
            if i_neighbours < 0 or i_neighbours >= self.row_number:
                continue
            for j_neighbours in range(cell.y - 1, cell.y + 2):
                if j_neighbours < 0 or j_neighbours >= self.col_number:
                    continue
                if i_neighbours != cell.x or j_neighbours != cell.y:
                    neighbours.append(self.cells[i_neighbours * self.col_number + j_neighbours])
        return neighbours

    def __iter__(self):
        self.element_number = 0
        return self

    def __next__(self):
        if self.element_number < self.row_number * self.col_number:
            element = self.cells[self.element_number]
            self.element_number += 1
            return element
        else:
            raise StopIteration()
        pass

    def __str__(self):
        list_cell = ""
        current_col = 0
        for cell in self.cells:
            list_cell += " {}".format(cell.is_alive())
            current_col += 1
            if current_col == self.col_number:
                current_col = 0
                list_cell += "\n"
        return list_cell


class GameOfLife:
    def __init__(self, width=640, height=480, cell_size=10, speed=10, filename=""):
        self.width = width
        self.height = height
        self.cell_size = cell_size

        # Устанавливаем размер окна
        self.screen_size = width, height
        # Создание нового окна
        self.screen = pygame.display.set_mode(self.screen_size)

        # Вычисляем количество ячеек по вертикали и горизонтали
        self.cell_width = self.width // self.cell_size
        self.cell_height = self.height // self.cell_size

        # Скорость протекания игры
        self.speed = speed

        self.cell_list = CellList(self.cell_width, self.cell_height)
        if filename != "":
            states = [int(c) for c in open(filename).read() if c in '01']
            position = 0
            for cell in self.cell_list:
                if position < len(states):
                    cell.state = states[position]
                    position += 1
        else:
            for cell in self.cell_list:
                cell.state = random.randint(0, 1)
    pass


    def draw_grid(self):
        # http://www.pygame.org/docs/ref/draw.html#pygame.draw.line
        for x in range(0, self.width, self.cell_size):
            pygame.draw.line(self.screen, pygame.Color('black'),
                             (x, 0), (x, self.height))
        for y in range(0, self.height, self.cell_size):
            pygame.draw.line(self.screen, pygame.Color('black'),
                             (0, y), (self.width, y))

    def run(self):
        pygame.init()
        clock = pygame.time.Clock()
        pygame.display.set_caption('Game of Life')
        self.screen.fill(pygame.Color('white'))
        self.draw_cell_list(self.cell_list)
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == QUIT:
                    running = False
            self.draw_grid()
            self.cell_list.update()
            game.draw_cell_list(self.cell_list)
            pygame.display.flip()
            clock.tick(self.speed)
        pygame.quit()

    def draw_cell_list(self, rects):
        for cell in rects:
            cell_color = pygame.Color('green') if cell.is_alive() else pygame.Color('white')
            pygame.draw.rect(self.screen,
                             cell_color,
                             (cell.x * self.cell_size + 1,
                              cell.y * self.cell_size + 1,
                              self.cell_size - 1,
                              self.cell_size - 1))

if __name__ == '__main__':
    game = GameOfLife(640, 480, 20, 5, filename="test.txt")
    game.run()
