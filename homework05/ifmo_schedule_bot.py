import datetime
import os
import sys

import telebot
import requests
import time
from bs4 import BeautifulSoup
from pathlib import Path

access_token = '337597542:AAGCiUkMV7oKV8Q0Qc7KO5q37rbPDus8LPg'
domain = 'http://www.ifmo.ru/ru/schedule/0'
# Создание бота с указанным токеном доступа
bot = telebot.TeleBot(access_token)

days = {
    'monday': '1',
    'tuesday': '2',
    'wednesday': '3',
    'thursday': '4',
    'friday': '5',
    'saturday': '6'
}

days_localized = {
    'понедельник': 0,
    'вторник': 1,
    'среда': 2,
    'четверг': 3,
    'пятница': 4,
    'суббота': 5,
    'воскресенье': 6
}


def get_page(group, week=''):
    path = 'cache/{}_{}.txt'.format(group, week)
    my_file = Path(path)
    if my_file.is_file():
        file_time = os.path.getmtime(path)
        now_time = time.time()
        if now_time - file_time < 1 * 60 * 60:
            f = open(path, 'rb')
            web_page = f.read()
            return web_page
    if week and week != '0':
        week = '/' + str(week)
    else:
        week = ''
    url = '{domain}/{group}{week}/raspisanie_zanyatiy_{group}.htm'.format(
        domain=domain,
        week=week,
        group=group.upper())
    response = requests.get(url)
    web_page = response.text.encode(sys.stdout.encoding, errors='replace')
    if len(get_all_schedule(web_page)) > 0:
        f = open(path, 'wb')
        f.write(web_page)
    return web_page


def get_all_schedule(web_page):
    soup = BeautifulSoup(web_page, "html5lib")

    schedule_table = soup.find("article", attrs={"class": 'content_block'})

    if schedule_table is None:
        return []

    days_list = schedule_table.find_all("h4", attrs={"class": 'rasp_day_mobile'})
    days_list = [day.text for day in days_list]

    schedule_list = []

    for i in range(1, 7):
        times_lst, locations_lst, lessons_lst = get_day_schedule(web_page, str(i))
        if len(times_lst) > 0:
            schedule_list.append([days_list.pop(0), times_lst, locations_lst, lessons_lst])
    return schedule_list


def get_day_schedule(web_page, day='1'):
    soup = BeautifulSoup(web_page, "html5lib")

    # Получаем таблицу с расписанием на понедельник
    schedule_table = soup.find("table", attrs={"id": '{}day'.format(day)})

    if schedule_table is None:
        return [], [], []

    # Время проведения занятий
    times_list = schedule_table.find_all("td", attrs={"class": "time"})
    times_list = [time.span.text + ' ' + time.find("dt").text for time in times_list]

    # Место проведения занятий
    locations_list = schedule_table.find_all("td", attrs={"class": "room"})
    locations_list = [room.span.text for room in locations_list]

    # Название дисциплин и имена преподавателей
    lessons_list = schedule_table.find_all("td", attrs={"class": "lesson"})
    lessons_list = [lesson.text.split() for lesson in lessons_list]
    lessons_list = [' '.join([info for info in lesson_info if info]) for lesson_info in
                    lessons_list]

    return times_list, locations_list, lessons_list


@bot.message_handler(commands=['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'])
def get_day(message):
    if len(message.text.split()) < 3:
        bot.send_message(message.chat.id, 'Неверная команда!\nПоследовательность ввода: DAY WEEK_NUMBER GROUP_NUMBER',
                         parse_mode='HTML')
        return
    day, week_number, group = message.text.split()
    if len(week_number) > 1:
        bot.send_message(message.chat.id, 'Неверная команда!\nПоследовательность ввода: DAY WEEK_NUMBER GROUP_NUMBER',
                         parse_mode='HTML')
        return
    web_page = get_page(group.upper(), week_number)
    day = ''.join(day.split('/'))
    times_lst, locations_lst, lessons_lst = get_day_schedule(web_page, days[day])

    resp = ''
    for time, location, lession in zip(times_lst, locations_lst, lessons_lst):
        resp += '<b>{}</b>, {}, {}\n\n'.format(time, location, lession)
    if resp:
        bot.send_message(message.chat.id, resp, parse_mode='HTML')
    else:
        bot.send_message(message.chat.id, 'Расписание не найдено!', parse_mode='HTML')


@bot.message_handler(commands=['all'])
def get_all(message):
    if len(message.text.split()) < 3:
        bot.send_message(message.chat.id, 'Неверная команда!\nПоследовательность ввода: /all WEEK_NUMBER GROUP_NUMBER',
                         parse_mode='HTML')
        return
    _, week_number, group = message.text.split()

    web_page = get_page(group.upper(), week_number)
    schedule_list = get_all_schedule(web_page)
    rasp = ''

    for day in schedule_list:
        rasp += '<b>{}</b>\n------------------\n'.format(day[0].upper())
        for time, location, lession in zip(day[1], day[2], day[3]):
            rasp += '<b>{}</b>, {}, {}\n\n'.format(time, location, lession)
    if rasp:
        bot.send_message(message.chat.id, rasp, parse_mode='HTML')
    else:
        bot.send_message(message.chat.id, 'Расписание не найдено!', parse_mode='HTML')


@bot.message_handler(commands=['near_lesson'])
def near_lesson(message):
    if len(message.text.split()) < 2:
        bot.send_message(message.chat.id, 'Неверная команда!\nПоследовательность ввода: /near_lesson GROUP_NUMBER',
                         parse_mode='HTML')
        return
    _, group = message.text.split()

    web_page = get_page(group.upper())
    schedule_list = get_all_schedule(web_page)
    rasp = ''
    date = datetime.datetime.today()
    current_day = date.weekday() + 6
    week = date.isocalendar()[1] + (1 if current_day == 6 else 0)
    week_string = 'четная' if week % 2 == 1 else 'нечетная'
    started_index = 0
    for day in schedule_list:
        day_number = days_localized[day[0].lower()]
        if day_number >= current_day:
            break
        else:
            started_index += 1
    arr_length = len(schedule_list)
    sorted_schedule = schedule_list[started_index:arr_length] + schedule_list[0:started_index]
    for day in schedule_list:
        for time, location, lession in zip(day[1], day[2], day[3]):
            times_week = time.split(' ')
            if len(times_week) == 2:
                times = times_week[0].split('-')
                if len(times) == 2:
                    pass
                if [times_week[1].lower() == week_string]:
                    print(123)
                else:
                    continue

    print(schedule_list)


@bot.message_handler(commands=['tomorrow'])
def near_lesson(message):
    if len(message.text.split()) < 2:
        bot.send_message(message.chat.id, 'Неверная команда!\nПоследовательность ввода: /tomorrow GROUP_NUMBER',
                         parse_mode='HTML')
        return
    _, group = message.text.split()
    date = datetime.datetime.today()
    current_day = date.weekday()
    tommorow_day = ((current_day + 1) % 7) + 1
    week = date.isocalendar()[1] + (1 if tommorow_day == 1 else 0)
    week_number = 1 if week % 2 == 1 else 2
    web_page = get_page(group, str(week_number))
    times_lst, locations_lst, lessons_lst = get_day_schedule(web_page, str(tommorow_day))
    rasp = ''
    for time, location, lession in zip(times_lst, locations_lst, lessons_lst):
        rasp += '<b>{}</b>, {}, {}\n\n'.format(time, location, lession)
    if rasp:
        bot.send_message(message.chat.id, rasp, parse_mode='HTML')
    else:
        bot.send_message(message.chat.id, 'Завтра занятий нет!', parse_mode='HTML')


if __name__ == '__main__':
    bot.polling(none_stop=True)
