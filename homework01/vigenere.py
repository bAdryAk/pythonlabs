def get_shift(letter):
    if letter.isupper():
        return ord(letter) - ord("A")
    else:
        return ord(letter) - ord("a")

def encrypt_vigenere(plaintext, keyword):
    """
    Encrypts plaintext using a Vigenere cipher.

    >>> encrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> encrypt_vigenere("python", "a")
    'python'
    >>> encrypt_vigenere("ATTACKATDAWN", "LEMON")
    'LXFOPVEFRNHR'
    """
    ciphertext = ""
    for i in range(plaintext.__len__()):
        shift = get_shift(keyword[i % keyword.__len__()])
        c = plaintext[i]
        if c.isupper():
            ciphertext += chr((ord(c) - ord("A") + shift) % (ord("Z") - ord("A") + 1) + ord("A"))
        else:
            ciphertext += chr((ord(c) - ord("a") + shift) % (ord("z") - ord("a") + 1) + ord("a"))
    return ciphertext

def decrypt_vigenere(ciphertext, keyword):
    """
    Decrypts a ciphertext using a Vigenere cipher.

    >>> decrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> decrypt_vigenere("python", "a")
    'python'
    >>> decrypt_vigenere("LXFOPVEFRNHR", "LEMON")
    'ATTACKATDAWN'
    """
    plaintext = ""
    for i in range(ciphertext.__len__()):
        shift = get_shift(keyword[i % keyword.__len__()])
        c = ciphertext[i]
        if c.isupper():
            plaintext += chr((ord(c) - ord("A") - shift) % (ord("Z") - ord("A") + 1) + ord("A"))
        else:
            plaintext += chr((ord(c) - ord("a") - shift) % (ord("z") - ord("a") + 1) + ord("a"))
    return plaintext