def encrypt_caesar(plaintext, shift):
    """
    Encrypts plaintext using a Caesar cipher.

    >>> encrypt_caesar("PYTHON", 29)
    'SBWKRQ'
    >>> encrypt_caesar("python", 3)
    'sbwkrq'
    >>> encrypt_caesar("", 10)
    ''
    """
    shift = shift % (ord("Z") - ord("A") + 1)
    ciphertext = ""
    for c in plaintext:
        if c.isalpha():
            if c.isupper():
                ciphertext += chr((ord(c) - ord("A") + shift) % (ord("Z") - ord("A") + 1) + ord("A"))
            else:
                ciphertext += chr((ord(c) - ord("a") + shift) % (ord("z") - ord("a") + 1) + ord("a"))
    return ciphertext


def decrypt_caesar(ciphertext, shift):
    """
    Decrypts a ciphertext using a Caesar cipher.

    >>> decrypt_caesar("SBWKRQ", 13003)
    'PYTHON'
    >>> decrypt_caesar("sbwkrq", 3)
    'python'
    >>> decrypt_caesar("", 10)
    ''
    """
    shift = shift % (ord("Z") - ord("A") + 1)
    plaintext = ""
    for c in ciphertext:
        if c.isalpha():
            if c.isupper():
                plaintext += chr((ord(c) - ord("A") - shift) % (ord("Z") - ord("A") + 1) + ord("A"))
            else:
                plaintext += chr((ord(c) - ord("a") - shift) % (ord("z") - ord("a") + 1) + ord("a"))
    # PUT YOUR CODE HERE
    return plaintext